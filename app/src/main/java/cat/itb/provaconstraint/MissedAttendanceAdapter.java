package cat.itb.provaconstraint;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.provaconstraint.R;


public class MissedAttendanceAdapter extends RecyclerView.Adapter<MissedAttendanceAdapter.MissedAttendanceViewHolder>{
    List<MissedAttendance> missedAttendances;

    public MissedAttendanceAdapter(List<MissedAttendance> missedAttendances) {
        this.missedAttendances = missedAttendances;
    }

    @NonNull
    @Override
    public MissedAttendanceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.missed_attendance_list_item, parent, false);

        return new MissedAttendanceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MissedAttendanceViewHolder holder, int position) {
        holder.bindData(missedAttendances.get(position));
    }

    @Override
    public int getItemCount() {
        return missedAttendances.size();
    }

    class MissedAttendanceViewHolder extends RecyclerView.ViewHolder{
        TextView studentName;
        TextView courseName;
        ImageView image;

        public MissedAttendanceViewHolder(@NonNull View itemView) {
            super(itemView);

            studentName = itemView.findViewById(R.id.textViewModel);
            courseName = itemView.findViewById(R.id.textView02);
            image = itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavDirections navDirections = MissedAttendanceListFragmentDirections.actionListToFragment(missedAttendances.get(getAdapterPosition()));
                    Navigation.findNavController(v).navigate(navDirections);
                }
            });
        }

        public void bindData(MissedAttendance missedAttendance){
            for (int i = 0; i < 100; i++) {
                studentName.setText(missedAttendance.getName());
                courseName.setText(missedAttendance.getModul());
                if (missedAttendance.isJustified()) image.setImageResource(R.mipmap.ic_check);
                else image.setImageResource(R.mipmap.ic_cross);
            }
        }
    }
}
