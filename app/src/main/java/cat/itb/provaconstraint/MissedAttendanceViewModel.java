package cat.itb.provaconstraint;

import android.content.res.Resources;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import android.content.res.Resources;

public class MissedAttendanceViewModel extends ViewModel {
    Date data = new Date();
    List<MissedAttendance> missedAttendances = new ArrayList<>();

    public MissedAttendanceViewModel(){
        if (missedAttendances.isEmpty()){
            for (int i = 1; i < 101; i++){
                MissedAttendance mA = new MissedAttendance();
                mA.setName("Student #" + i);
                mA.setDate(data);
                if (i%2==0) mA.setJustified(true);
                else mA.setJustified(false);
                missedAttendances.add(mA);
            }
        }
    }

}
