package cat.itb.provaconstraint;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class MissedAttendance implements Parcelable {
    private String name;
    private String modul;
    private Date date;
    private boolean justified;

    public MissedAttendance() {
        this.name = "";
        this.modul = "";
        this.date = new Date();
        this.justified = false;
    }

    protected MissedAttendance(Parcel in) {
        name = in.readString();
        modul = in.readString();
        justified = in.readByte() != 0;
    }

    public static final Creator<MissedAttendance> CREATOR = new Creator<MissedAttendance>() {
        @Override
        public MissedAttendance createFromParcel(Parcel in) {
            return new MissedAttendance(in);
        }

        @Override
        public MissedAttendance[] newArray(int size) {
            return new MissedAttendance[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModul() {
        return modul;
    }

    public void setModul(String modul) {
        this.modul = modul;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isJustified() {
        return justified;
    }

    public void setJustified(boolean justified) {
        this.justified = justified;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(modul);
        dest.writeByte((byte) (justified ? 1 : 0));
    }
}
