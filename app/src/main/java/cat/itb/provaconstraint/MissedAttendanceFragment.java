package cat.itb.provaconstraint;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import java.util.Date;

import cat.itb.provaconstraint.R;

public class MissedAttendanceFragment extends Fragment {

    MissedAttendance missedAttendance;
    EditText studentNameText;
    Spinner spinner;
    Button hour;
    CheckBox justified;
    Button add;
    String name;
    String spinnerText;
    String justifiedField;
    Date data = new Date();
    public String text;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.missed_attendance_fragment, container, false);


        studentNameText = v.findViewById(R.id.editTextPersonName);
        spinner = v.findViewById(R.id.spinner);
        hour = v.findViewById(R.id.button1);
        add = v.findViewById(R.id.button2);
        justified = v.findViewById(R.id.checkBox);

        if (getArguments() != null) missedAttendance = getArguments().getParcelable("missedAttendance");
        studentNameText.setText(missedAttendance.getName());
        justified.setSelected(missedAttendance.isJustified());



        hour.setText(data.toString());

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = studentNameText.getText().toString();
                hour.setText(data.toString());
                spinnerText = spinner.getSelectedItem().toString();
                if (justified.isSelected()) justifiedField = " with justification.";
                else justifiedField = " with no justification.";
                text = "The student " + name + " has missed " + spinnerText + " on " + data.toString() + justifiedField;
                AlertDialog(v);
            }
        });

        return v;
    }

    public void AlertDialog(final View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Missed attendance created");
        builder.setMessage(text);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                NavDirections navDirections = MissedAttendanceFragmentDirections.actionMissedAttendanceFragmentToMissedAttendanceListFragment(missedAttendance);
                Navigation.findNavController(view).navigate(navDirections);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
